FROM python:3.12-alpine

# renovate: datasource=pypi depName=kopf
ENV KOPF_VERSION 1.37.1

RUN apk add fping=5.1-r5 --no-cache

# hadolint ignore=DL3013
RUN pip install --no-cache-dir kopf==${KOPF_VERSION} \
  && pip install --no-cache-dir kubernetes pyyaml requests prometheus_client